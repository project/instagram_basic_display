<?php

declare(strict_types=1);

namespace Drupal\instagram_basic_display\Plugin\Network;

use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Url;
use Drupal\instagram_basic_display\Exception\InstagramApiException;
use Drupal\instagram_basic_display\Exception\InstagramNoTokenException;
use Drupal\instagram_basic_display\Media;
use Drupal\instagram_basic_display\MediaSet;
use Drupal\social_api\Plugin\NetworkBase;
use Drupal\social_api\SocialApiException;
use Drupal\instagram_basic_display\Settings\InstagramAuthnSettings;
use Drupal\social_auth\Entity\SocialAuth;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\BadResponseException;
use GuzzleHttp\RequestOptions;
use League\OAuth2\Client\Provider\Instagram as InstagramClient;
use League\OAuth2\Client\Token\AccessToken;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Response as SymfonyResponse;

/**
 * Defines a Network Plugin for Instagram Basic Display API.
 *
 * @Network(
 *  id = "instagram_basic_display",
 *  social_network = "Instagram",
 *  type = "social_network",
 *  handlers = {
 *   "settings": {
 *    "class": "\Drupal\instagram_basic_display\Settings\InstagramAuthnSettings",
 *    "config_id": "instagram_basic_display.settings"
 *    }
 *  }
 * )
 */
class Instagram extends NetworkBase implements InstagramInterface {

  /**
   * {@inheritDoc}
   */
  public function getSdk(): InstagramClient {
    return parent::getSdk();
  }

  /**
   * HTTP client.
   *
   * @var \GuzzleHttp\Client
   */
  protected $client;

  /**
   * Current user.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $currentUser;

  /**
   * Setter for the current user.
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   */
  public function setCurrentUser(AccountInterface $account) {
    $this->currentUser = $account;
  }

  /**
   * Setter for the HTTP client.
   *
   * @param \GuzzleHttp\Client $client
   */
  public function setHttpClient(Client $client) {
    $this->client = $client;
  }

  /**
   * {@inheritDoc}
   */
  public static function create(
    ContainerInterface $container,
    array $configuration,
    $plugin_id,
    $plugin_definition
  ) {
    $instance = parent::create(
      $container,
      $configuration,
      $plugin_id,
      $plugin_definition
    );
    $instance->setHttpClient($container->get('http_client'));
    $instance->setCurrentUser($container->get('current_user'));
    return $instance;
  }

  /**
   * Sets the underlying SDK library.
   *
   * @return \League\OAuth2\Client\Provider\Instagram|false
   *   The initialized 3rd party library instance.
   *   False if library could not be initialized.
   *
   * @throws \Drupal\social_api\SocialApiException
   *   If the SDK library does not exist.
   */
  protected function initSdk() {
    $class_name = '\League\OAuth2\Client\Provider\Instagram';
    if (!class_exists($class_name)) {
      throw new SocialApiException(
        sprintf(
          'The PHP League OAuth2 library for Instagram not found. Class: %s.',
          $class_name
        )
      );
    }

    /** @var \Drupal\instagram_basic_display\Settings\InstagramAuthnSettings $settings */
    $settings = $this->settings;
    if ($this->validateConfig($settings)) {
      // All these settings are mandatory.
      $league_settings = [
        'clientId' => $settings->getClientId(),
        'clientSecret' => $settings->getClientSecret(),
        'redirectUri' => Url::fromRoute('instagram_basic_display.callback')
          ->setAbsolute()->toString(TRUE)->getGeneratedUrl(),
      ];

      return new InstagramClient(
        $league_settings,
        ['httpClient' => $this->client]
      );
    }

    return FALSE;
  }

  /**
   * Checks that module is configured.
   *
   * @param \Drupal\instagram_basic_display\Settings\InstagramAuthnSettings $settings
   *   The Instagram auth settings.
   *
   * @return bool
   *   True if module is configured.
   *   False otherwise.
   */
  protected function validateConfig(InstagramAuthnSettings $settings) {
    $client_id = $settings->getClientId();
    $client_secret = $settings->getClientSecret();
    if (!$client_id || !$client_secret) {
      $this->loggerFactory
        ->get('social_auth_instagram')
        ->error('Define Client ID and Client Secret on module settings.');

      return FALSE;
    }

    return TRUE;
  }

  /**
   * Perform a Graph API call.
   *
   * @param string $endpoint
   *   Endpoint to call.
   * @param string $token
   *   Access token.
   * @param array $args
   *   Query parameters to include.
   * @param string $method
   *   Method, defaults to GET.
   * @param array $options
   *   Additional request options.
   *
   * @return \Psr\Http\Message\ResponseInterface
   *   The response.
   * @throws \Drupal\instagram_basic_display\Exception\InstagramApiException
   */
  protected function callApi(
    string $endpoint,
    string $token,
    array $args = [],
    $method = 'GET',
    $options = []
  ) {
    try {
      return $this->client->request(
        $method,
        $this->getSdk()->getGraphHost() . '/' . $endpoint,
        [RequestOptions::QUERY => ['access_token' => $token] + $args] + $options,
      );
    } catch (BadResponseException $e) {
      // Instagram/Facebook don't put much meaning into HTTP response codes.
      try {
        $message = json_decode(
          $e->getResponse()->getBody()->getContents(),
          flags: JSON_THROW_ON_ERROR,
        );
        // @see https://developers.facebook.com/docs/graph-api/guides/error-handling#receiving-errorcodes
        if (!empty($message->error)) {
          throw new InstagramApiException(
            $message->error->message,
            $message->error->code,
            $e
          );
        }
      } catch (\InvalidArgumentException $x) {
        throw new InstagramApiException(
          $e->getMessage(),
          $e->getResponse()->getStatusCode(),
          $e
        );
      }
    }
  }

  protected function getMedia(
    string $token,
    array $options = [],
    ?int $id = NULL
  ): MediaSet {
    $response = $this->callApi(
      $id ? (string) $id : 'me/media',
      $token,
      [
        'fields' => implode(
          ',',
          [
            'caption',
            'id',
            'media_type',
            'media_url',
            'timestamp',
            'thumbnail_url'
          ]
        )
      ] + $options
    );
    $payload = json_decode($response->getBody()->getContents(), flags: JSON_THROW_ON_ERROR);
    return new MediaSet(
      $payload->data ?? [$payload],
      $this
    );
  }

  /**
   * {@inheritDoc}
   */
  public function getMediaFromUrl(Url $url): MediaSet {
    assert(array_key_exists('access_token', $url->getOption('query')));
    $query = $url->getOption('query');
    unset($query['fields']);
    return $this->getMedia($query['access_token']);
  }

  /**
   * {@inheritDoc}
   */
  public function getLongLivedToken(AccessToken $short_lived): AccessToken {
    $token = json_decode(
      $this->callApi(
        'access_token',
        $short_lived->getToken(),
        [
          'grant_type' => 'ig_exchange_token',
          'client_secret' => $this->settings->getClientSecret(),
        ]
      )->getBody()->getContents(),
      flags: JSON_THROW_ON_ERROR,
    );
    return new AccessToken([
      'access_token' => $token->access_token,
      'expires_in' => $token->expires_in,
    ]);
  }

  /**
   * {@inheritDoc}
   */
  public function testMediaAccess(string $token): bool {
    return $this->callApi(
        'me/media',
        $token,
        [],
        'HEAD',
        [RequestOptions::HTTP_ERRORS => FALSE]
      )
        ->getStatusCode() === SymfonyResponse::HTTP_OK;
  }

  protected function getUserAuth(?AccountInterface $user = NULL): SocialAuth {
    if (!$user) {
      $user = $this->currentUser;
    }
    $auth = $this->entityTypeManager->getStorage('social_auth')
      ->loadByProperties([
        'plugin_id' => $this->getPluginId(),
        'user_id' => $user->id(),
      ]);
    if (count($auth) !== 1) {
      throw new InstagramNoTokenException();
    }
    /** @var \Drupal\social_auth\Entity\SocialAuth $auth */
    return current($auth);
  }

  /**
   * {@inheritDoc}
   */
  public function getMediaForUser(
    ?AccountInterface $user = NULL,
    array $options = []
  ): MediaSet {
    return $this->getMedia($this->getUserAuth($user)->getToken(), $options);
  }

  /**
   * {@inheritDoc}
   */
  public function getMediaById($id, ?AccountInterface $user = NULL, array $options = []): Media {
    $mediaSet = $this->getMedia(
      $this->getUserAuth($user)->getToken(),
      $options,
      (int) $id
    );
    return $mediaSet->current();
  }

  /**
   * {@inheritDoc}
   */
  public function getRedirectPath(): string {
    /** @var \Drupal\instagram_basic_display\Settings\InstagramAuthnSettingsInterface $settings */
    $settings = $this->settings;
    return $settings->getRedirectPath();
  }

}
