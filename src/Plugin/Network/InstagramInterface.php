<?php

declare(strict_types=1);

namespace Drupal\instagram_basic_display\Plugin\Network;

use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Url;
use Drupal\instagram_basic_display\Media;
use Drupal\instagram_basic_display\MediaSet;
use League\OAuth2\Client\Token\AccessToken;

interface InstagramInterface {

  /**
   * Get media for a user, whose access token will be retrieved.
   *
   * @param \Drupal\Core\Session\AccountInterface|NULL $user
   *   The user.
   * @param array $options
   *   Options.
   *
   * @return \Drupal\instagram_basic_display\MediaSet
   *   Media set.
   * @throws \Drupal\instagram_basic_display\Exception\InstagramNoTokenException
   */
  public function getMediaForUser(?AccountInterface $user = NULL, array $options = []): MediaSet;

  /**
   * Get media from a URL, helpful for following pagination links.
   *
   * @param \Drupal\Core\Url $url
   *   URL object; the host and path will be overridden.
   *
   * @return \Drupal\instagram_basic_display\MediaSet
   *   Media set.
   */
  public function getMediaFromUrl(Url $url): MediaSet;

  /**
   * Get media by ID.
   *
   * @param mixed $id
   *   Instagram media ID.
   * @param \Drupal\Core\Session\AccountInterface|NULL $user
   *   User
   * @param array $options
   *   Query parameter options for fetch.
   *
   * @return \Drupal\instagram_basic_display\Media
   */
  public function getMediaById($id, ?AccountInterface $user = NULL, array $options = []): Media;

  /**
   * Perform a HEAD request on the media endpoint, to test if the user
   * granted sufficient scope to access their media.
   *
   * @param string $token
   *   Access token.
   *
   * @return bool
   *   Whether we have sufficient scope to access media.
   */
  public function testMediaAccess(string $token): bool;

  /**
   * Exchange a short-lived token for a long-lived token.
   *
   * @see https://developers.facebook.com/docs/instagram-basic-display-api/reference/access_token
   *
   * @param \League\OAuth2\Client\Token\AccessToken $short_lived
   *   Short-lived token.
   *
   * @return \League\OAuth2\Client\Token\AccessToken
   *   Long-lived token.
   */
  public function getLongLivedToken(AccessToken $short_lived): AccessToken;

  /**
   * Get the redirect path from the settings.
   *
   * @return string
   */
  public function getRedirectPath(): string;

}
