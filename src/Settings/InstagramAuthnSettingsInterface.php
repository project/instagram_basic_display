<?php

namespace Drupal\instagram_basic_display\Settings;

/**
 * Defines an interface for Instagram Basic Display settings.
 */
interface InstagramAuthnSettingsInterface {

  /**
   * Gets the client ID.
   *
   * @return string
   *   The client ID.
   */
  public function getClientId();

  /**
   * Gets the client secret.
   *
   * @return string
   *   The client secret.
   */
  public function getClientSecret();

  /**
   * Gets the path to redirect the user on success or failure.
   *
   * @return string
   */
  public function getRedirectPath(): string;

}
