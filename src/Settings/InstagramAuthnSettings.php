<?php

namespace Drupal\instagram_basic_display\Settings;

use Drupal\social_api\Settings\SettingsBase;

/**
 * Defines methods to get Social Auth Instagram settings.
 */
class InstagramAuthnSettings extends SettingsBase implements InstagramAuthnSettingsInterface {

  /**
   * Client ID.
   *
   * @var string
   */
  protected $clientId;

  /**
   * Client secret.
   *
   * @var string
   */
  protected $clientSecret;

  /**
   * Redirect path.
   *
   * @var string
   */
  protected $redirectPath;

  /**
   * {@inheritdoc}
   */
  public function getClientId() {
    if (!$this->clientId) {
      $this->clientId = $this->config->get('client_id');
    }
    return $this->clientId;
  }

  /**
   * {@inheritdoc}
   */
  public function getClientSecret() {
    if (!$this->clientSecret) {
      $this->clientSecret = $this->config->get('client_secret');
    }
    return $this->clientSecret;
  }

  /**
   * {@inheritDoc}
   */
  public function getRedirectPath(): string {
    if (!$this->redirectPath) {
      $this->redirectPath = $this->config->get('redirect_path');
    }
    return $this->redirectPath;
  }


}
