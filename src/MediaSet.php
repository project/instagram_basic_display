<?php

declare(strict_types=1);

namespace Drupal\instagram_basic_display;

use Drupal\Core\Url;
use Drupal\instagram_basic_display\Plugin\Network\InstagramInterface;

/**
 * Value object for a set of Instagram media.
 *
 * @see https://developers.facebook.com/docs/graph-api/results#cursors
 */
final class MediaSet extends \ArrayIterator {

  /**
   * Instagram plugin.
   *
   * @var \Drupal\instagram_basic_display\Plugin\Network\InstagramInterface
   */
  protected $plugin;

  /**
   * Next page.
   *
   * @var \Drupal\Core\Url
   */
  protected Url $next;

  /**
   * Previous page.
   *
   * @var \Drupal\Core\Url
   */
  protected Url $previous;

  /**
   * {@inheritDoc}
   */
  public function __construct(array $data, InstagramInterface $plugin) {
    $this->plugin = $plugin;
    $media = [];
    foreach ($data as $apiMedia) {
      $media[] = new Media($apiMedia);
    }
    if (!empty($apiResponse->paging)) {
      foreach (['next', 'previous'] as $pagerLink) {
        if (!empty($apiResponse->paging->{$pagerLink})) {
          $this->{$pagerLink} = Url::fromUri($apiResponse->paging->{$pagerLink});
        }
      }
    }
    parent::__construct($media);
  }

  /**
   * Determines if there are more results.
   *
   * @return bool
   */
  public function hasMore(): bool {
    return !is_null($this->next) || !is_null($this->previous);
  }

  /**
   * Get a paged result in a specified direction.
   *
   * @param string $key
   *   Pager direction.
   *
   * @return \Drupal\instagram_basic_display\MediaSet
   *   Media set.
   */
  protected function getPaged(string $key): MediaSet {
    if (is_null($this->{$key})) {
      throw new \InvalidArgumentException(sprintf('Page %s does not exist.', $key));
    }
    return $this->plugin->getMediaFromUrl($this->{$key});
  }

  /**
   * Get previous page.
   *
   * @return \Drupal\instagram_basic_display\MediaSet
   */
  public function getPrevious(): MediaSet {
    return $this->getPaged('previous');
  }

  /**
   * Get next page.
   *
   * @return \Drupal\instagram_basic_display\MediaSet
   */
  public function getNext(): MediaSet {
    return $this->getPaged('next');
  }

  /**
   * Get the value for 'before' pagination parameter, if there is a previous
   * pagination link.
   *
   * @return string|null
   *   The cursor.
   */
  public function getPreviousBeforeCursor(): ?string {
    if ($this->previous) {
      return $this->previous->getOption('query')['before'];
    }
    return NULL;
  }

  /**
   * Get the value for 'after' pagination parameter, if there is a next
   * pagination link.
   *
   * @return string|null
   *   The cursor.
   */
  public function getNextAfterCursor(): ?string {
    if ($this->next) {
      return $this->next->getOption('query')['after'];
    }
    return NULL;
  }

}
