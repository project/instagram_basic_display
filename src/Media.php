<?php

declare(strict_types=1);

namespace Drupal\instagram_basic_display;

/**
 * Value object for Instagram Media.
 */
final class Media {

  /**
   * Media ID.
   *
   * @var string
   */
  protected string $id;

  /**
   * Media caption.
   *
   * @var string|NULL
   */
  protected ?string $caption = NULL;

  /**
   * Media type.
   *
   * @var string
   */
  protected string $media_type;

  /**
   * Media URL.
   *
   * @var string
   */
  protected string $media_url;

  /**
   * Constructor.
   *
   * @param object $apiMedia
   *   Media object, as retrieved from the Instagram API.
   */
  public function __construct(object $apiMedia) {
    assert(!empty($apiMedia->media_url));
    foreach (['id', 'caption', 'media_type', 'media_url'] as $key) {
      if (!empty($apiMedia->{$key})) {
        $this->{$key} = $apiMedia->{$key};
      }
    }
  }

  /**
   * Get the media ID.
   *
   * @return int
   */
  public function getId(): int {
    return (int) $this->id;
  }

  /**
   * Get the media caption.
   *
   * @return string|NULL
   */
  public function getCaption(): ?string {
    return $this->caption;
  }

  /**
   * Get the media type.
   *
   * @return string
   */
  public function getMediaType(): string {
    return $this->media_type;
  }

  /**
   * Get the media URL.
   *
   * @return string
   */
  public function getMediaUrl(): string {
    return $this->media_url;
  }

}
