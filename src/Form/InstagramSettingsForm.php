<?php

namespace Drupal\instagram_basic_display\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Routing\RequestContext;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Settings form for Instagram Basic Display API.
 */
class InstagramSettingsForm extends ConfigFormBase {

  /**
   * Request context.
   *
   * @var \Drupal\Core\Routing\RequestContext
   */
  protected $requestContext;

  /**
   * Constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The factory for configuration objects.
   * @param \Drupal\Core\Routing\RequestContext $request_context
   *   Request context.
   */
  public function __construct(ConfigFactoryInterface $config_factory, RequestContext $request_context) {
    parent::__construct($config_factory);
    $this->requestContext = $request_context;
  }

  /**
   * {@inheritDoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('router.request_context')
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['instagram_basic_display.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'instagram_basic_display_settings';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('instagram_basic_display.settings');

    $form['instagram_settings'] = [
      '#type' => 'details',
      '#title' => $this->t('Instagram Client settings'),
      '#open' => TRUE,
      '#description' => $this->t('You need to first create a Instagram App at <a href="@instagram-dev">@instagram-dev</a>',
        ['@instagram-dev' => 'https://developers.facebook.com/docs/instagram-basic-display-api/']),
    ];

    $form['instagram_settings']['client_id'] = [
      '#type' => 'textfield',
      '#required' => TRUE,
      '#title' => $this->t('Client ID'),
      '#default_value' => $config->get('client_id'),
      '#description' => $this->t('Copy the Client ID here.'),
    ];

    $form['instagram_settings']['client_secret'] = [
      '#type' => 'textfield',
      '#required' => TRUE,
      '#title' => $this->t('Client Secret'),
      '#default_value' => $config->get('client_secret'),
      '#description' => $this->t('Copy the Client Secret here.'),
    ];
    $form['instagram_settings']['redirect_path'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Return Redirect Path'),
      '#default_value' => $config->get('redirect_path') ?? '/user',
      '#size' => 40,
      '#required' => TRUE,
      '#description' => $this->t(
        'Specify a relative URL to redirect the user on success or failure. Will have a <code>?success=[1|0]</code> query parameter applied. Useful when supporting <a href="@link">modal/JS callback workflows</a>.',
        ['@link' => 'https://stackoverflow.com/questions/7606071/how-can-i-do-oauth-request-by-open-new-window-instead-of-redirect-user-from-cur#comment9230934_7607058']
      ),
      '#field_prefix' => $this->requestContext->getCompleteBaseUrl(),
    ];
    $form['instagram_settings']['authorized_redirect_url'] = [
      '#type' => 'textfield',
      '#disabled' => TRUE,
      '#title' => $this->t('Valid redirect URIs'),
      '#description' => $this->t('Copy this value to <em>Valid redirect URIs</em> field of your Instagram App settings.'),
      '#default_value' => Url::fromRoute('instagram_basic_display.callback')->setAbsolute()->toString(),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * @inheritDoc
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    // Validate redirect path.
    if (($value = $form_state->getValue('redirect_path')) && $value[0] !== '/') {
      $form_state->setErrorByName('redirect_path', $this->t("The path '%path' has to start with a slash.", ['%path' => $form_state->getValue('site_frontpage')]));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    $this->config('instagram_basic_display.settings')
      ->set('client_id', $values['client_id'])
      ->set('client_secret', $values['client_secret'])
      ->set('redirect_path', $values['redirect_path'])
      ->save();

    parent::submitForm($form, $form_state);
  }

}
