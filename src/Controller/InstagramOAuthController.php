<?php

namespace Drupal\instagram_basic_display\Controller;

use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Render\RendererInterface;
use Drupal\Core\Url;
use Drupal\instagram_basic_display\Plugin\Network\InstagramInterface;
use Drupal\social_api\Plugin\NetworkManager;
use Drupal\social_auth\Controller\OAuth2ControllerBase;
use Drupal\social_auth\SocialAuthDataHandler;
use Drupal\social_auth\User\UserAuthenticator;
use Drupal\instagram_basic_display\InstagramOAuthManager;
use Drupal\social_auth\User\UserManager;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Response;

/**
 * Returns responses for Instagram OAuth2 routes.
 */
class InstagramOAuthController extends OAuth2ControllerBase {

  /**
   * User manager.
   *
   * @var \Drupal\social_auth\User\UserManager
   */
  protected $userManager;

  /**
   * Instagram plugin.
   *
   * @var \Drupal\instagram_basic_display\Plugin\Network\InstagramInterface
   */
  protected InstagramInterface $plugin;

  /**
   * InstagramAuthController constructor.
   *
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The messenger service.
   * @param \Drupal\social_api\Plugin\NetworkManager $network_manager
   *   Used to get an instance of the Instagram network plugin.
   * @param \Drupal\social_auth\User\UserAuthenticator $user_authenticator
   *   Manages user login/registration.
   * @param \Drupal\instagram_basic_display\InstagramOAuthManager $instagram_manager
   *   Used to manage authentication methods.
   * @param \Symfony\Component\HttpFoundation\RequestStack $request
   *   Used to access GET parameters.
   * @param \Drupal\social_auth\SocialAuthDataHandler $data_handler
   *   The Social Auth data handler.
   * @param \Drupal\Core\Render\RendererInterface $renderer
   *   Used to handle metadata for redirection to authentication URL.
   */
  public function __construct(MessengerInterface $messenger,
                              NetworkManager $network_manager,
                              UserAuthenticator $user_authenticator,
                              InstagramOAuthManager $instagram_manager,
                              RequestStack $request,
                              SocialAuthDataHandler $data_handler,
                              RendererInterface $renderer,
                              UserManager $user_manager) {

    parent::__construct('Instagram Basic Display API', 'instagram_basic_display',
                        $messenger, $network_manager, $user_authenticator,
                        $instagram_manager, $request, $data_handler, $renderer);
    $this->userManager = $user_manager;
    $this->plugin = $this->networkManager
      ->createInstance($this->userAuthenticator->getPluginId());
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('messenger'),
      $container->get('plugin.network.manager'),
      $container->get('social_auth.user_authenticator'),
      $container->get('instagram_basic_display.manager'),
      $container->get('request_stack'),
      $container->get('social_auth.data_handler'),
      $container->get('renderer'),
      $container->get('social_auth.user_manager')
    );
  }

  /**
   * Response for authorization OAuth2 callback.
   *
   * Instagram returns the user here after user has authorized Drupal.
   */
  public function callback() {
    // Checks if there was an error.
    $redirect = $this->checkAuthError();
    if ($redirect !== NULL) {
      return $this->getResponse(FALSE);
    }

    /** @var \League\OAuth2\Client\Provider\InstagramResourceOwner|null $profile */
    if (!$profile = $this->processCallback()) {
      return $this->getResponse(FALSE);
    }
    $token = $this->providerManager->getAccessToken();
    if (!$this->plugin->testMediaAccess($token->getToken())) {
      // The user declined access to media, which is kinda the whole point!
      $this->messenger()->addError($this->t('You must provide access to media to use these features.'));
      return $this->getResponse(FALSE);
    }
    $token = $this->plugin->getLongLivedToken($token);
    // The user manager only has a method to get the Drupal User ID.
    /** @var \Drupal\social_auth\Entity\SocialAuth[] $existing */
    $existing = $this->entityTypeManager()->getStorage('social_auth')
      ->loadByProperties([
        // Set through the constructor.
        'plugin_id' => $this->userAuthenticator->getPluginId(),
        'provider_user_id' => $profile->getId(),
      ]);
    if (count($existing) === 1) {
      // Update token.
      $authEntry = current($existing);
      $authEntry->setToken($token->getToken());
      $authEntry->setAdditionalData(
        ['expires' => $token->getExpires()] + $authEntry->getAdditionalData()
      );
      $authEntry->save();
    }
    else {
      // Add new record.
      $this->userManager->addUserRecord(
        $this->currentUser()->id(),
        $profile->getId(),
        $token,
        [],
      );
    }
    $this->messenger()->addStatus($this->t('Instagram account linked'));

    return $this->getResponse(TRUE);
  }

  /**
   * Get a response for a success condition.
   *
   * This method allows local implementations to customize the response,
   * e.g. if the authorization is performed in a pop-up window and the
   * response must contain JavaScript to call window.opener.
   *
   * @param bool $successful
   *   Whether the authorization was successful.
   *
   * @return \Symfony\Component\HttpFoundation\RedirectResponse
   *   Redirect response.
   */
  protected function getResponse(bool $successful): Response {
    $url = Url::fromUri(
      'internal:' . $this->plugin->getRedirectPath(),
      ['query' => ['success' => intval($successful)]]
    );
    return new RedirectResponse($url->setAbsolute()->toString(TRUE)->getGeneratedUrl());
  }

}
