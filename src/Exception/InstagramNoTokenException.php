<?php

declare(strict_types=1);

namespace Drupal\instagram_basic_display\Exception;

use Throwable;

/**
 * Exception to indicate the user has no token stored.
 */
final class InstagramNoTokenException extends InstagramApiException {

  /**
   * {@inheritDoc}
   */
  public function __construct($message = '', $code = 0, Throwable $previous = NULL) {
    parent::__construct(
      $message ?: 'Missing or invalid Instagram authorization.',
      $code,
      $previous
    );
  }

}
