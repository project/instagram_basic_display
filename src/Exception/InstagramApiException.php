<?php

declare(strict_types=1);

namespace Drupal\instagram_basic_display\Exception;

/**
 * Exception class for Instagram API errors.
 *
 * Common codes (these aren't documented by Facebook):
 *   - 100: Not found or no permission to access.
 */
class InstagramApiException extends \Exception {

}
