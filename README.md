# Instagram Basic Display API integration

## Dependencies

This module depends on `social_auth` module in order to leverage its storage API for
social network access tokens. However, [do not use](https://www.drupal.org/project/social_auth_instagram/issues/3232753)
Instagram's OAuth2 implementation for authentication, it is an explicit violation of their terms. Use
`social_auth_facebook` instead, if you're targeting Facebook users. This is for authorization, only. But since
the account link is very similar to authentication, we'll leverage the `social_auth` entity.
